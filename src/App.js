import React,{useState} from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/404Page';
import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext';

//For routes
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
//The Router(BrowserRouter) component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.

//The Routes(before it calls Switch) declares the Route we can go to. For example, when we want to visit the course page only.

function App() {

  //React context is nothing but a global state to the app. It is a way to make a particular data available to all the components no matter how they are nested.  Context helps you broadcast data and changes happening to that data/state to all components.
  const [user, setUser] = useState({
      accessToken: localStorage.getItem('accessToken'),
      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === 'true'
    })

  //Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />}/>
            <Route path="/courses" element={<Courses />}/>
            <Route path="/register" element={<Register />}/>
            <Route path="/login" element={<Login />}/>       
            <Route path="/logout" element={<Logout />}/>
            <Route path="*" element={<PageNotFound />} />       
          </Routes>  
        </Container>
      </Router>
      
    </UserProvider>

      );
}

export default App;

