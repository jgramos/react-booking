import React, {useState, useEffect} from 'react';
import {Row, Col, Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}) {
	//Decsontruct the courseProp into their own variables
	const { name, description, price } = courseProp;
	//Let's create our useState hooks to store its state
	//States are used to keep track of information related to individual components
	/*Syntax:
		const [currentValue(getter), updatedValue(setter)] = useState(InitialGetterValue)
	*/

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	//statehook that indicates availability of course for enrollment(enroll button)
	const [isOpen, setIsOpen] = useState(true);

	const enroll = (a) => {
		setCount(count +1)
		console.log("Enrollees: " + count);
		setSeats(seats -1);
		console.log("Seats: " + count);	
	}

	useEffect(() => {
		if(seats === 0){
			setIsOpen(false);
		}
	}, [seats])
	

	return(
		<Row className="mt-2">
			<Col xs={12}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>Php {price}</Card.Text>
						<Card.Text>Enrollees: {count}</Card.Text>
						<Card.Text>Seats: {seats}</Card.Text>
						{isOpen ?
							<Button variant="primary" onClick={enroll}>Enroll</Button>	
							:

							<Button variant="primary" disabled>Enroll</Button>	
						}

					</Card.Body>
					
				</Card>
			</Col>
		</Row>

		)
}

//Check if the CourseCard component is getting the correct prop types
//PropTypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.

CourseCard.propTypes = {
//shape() method - it is used to check if a prop object conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		//Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}