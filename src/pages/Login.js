import React, {useState, useEffect, useContext} from 'react';
//useContext is used to unpack or deconstruct the value of the UserContext
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';

export default function Login() {

	const navigate = useNavigate();

	//Allows us to consume the User context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext);
	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	//State to determine whether submit button is enabled or not for conditonal rendering

	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		//Validation to enable submit button when all fields are populated and both passwords match
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	/*Notes
		fetch() is a method in JS, which allows to send a request to an API and process its response.

		Syntax:

		fetch('url',{options}).then(response => response.json()).then(data => {console.log(data)};

		-'url' = the url coming from the API/server 
		-{optional object} = it contains additional information about our request such as method, body and headers (Content-TYpe: application/json) or any other info.
		-then(response => response.json()) = parse the response as JSON
		-.then(data => {console.log(data)} = process the actual data

	*/

	function authenticate(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({accessToken: data.accessToken});

				Swal.fire({
					title: "Buray Ni Ina!",
					icon:"success",
					text: "Totoo nga ang balita!"
				});

				//Getting the user's credentials
				fetch('http://localhost:4000/users/getUserDetails', {
					headers:{
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result)
					if(result.isAdmin === true) {
						localStorage.setItem('email', result.email)
						localStorage.setItem('isAdmin', result.isAdmin)
						setUser({
							email: result.email,
							isAdmin: result.isAdmin
						})

						//redirect the admin to /courses
						navigate('/courses')

					}else {
						//if not an admin, redirect to homepage
						navigate('/')
					}
				})

			} else{
				Swal.fire({
					title:'Ooops!',
					icon:'error',
					text:'Something Went Wrong. Check your Credentials'
				})
			}

			setEmail('');
			setPassword('');
		})

		/*	wala pang fetch()	

		//Clear inpupt fields
		setEmail('')
		setPassword1('')

		setUser({email: localStorage.setItem('email', email)
		});		

		Swal.fire({
			title: "Buray Ni Ina!",
			icon:"success",
			text: "Totoo nga ang balita!"
		});

		*/
	}

	return(
		(user.accessToken !== null) ?
		<Navigate to="/courses"/>
		:
		<Form className="mt-5 mb-5" onSubmit={(e) => authenticate(e)}>
			<h1>Log-in</h1>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>
			{isActive ?
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>
				:
				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
			}
			
		</Form>
	)
}